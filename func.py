def clear():
    os.system('cls' if os.name=='nt' else 'clear')
def input_safe(type_, prompt=''):
    try:
        return type_(input(prompt))
    except ValueError:
        return input_safe(type_, prompt)
