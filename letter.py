def menu(options: list, prompt=''):
    if prompt:
        print(prompt)
    for i, v in enumerate(options, 1):
        print(f'{i}: {v}')
    valid = range(1, len(options) + 1)
    index = input_safe(int, 'Select a number: ')
    if index not in valid:
        return menu(options)
    return index - 1
