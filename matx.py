def generate_matrix(n_cols, n_rows, start, step):
    flat = np.arange(start, (start + n_rows * n_cols * step), step)
    return flat.reshape((n_rows, n_cols))

