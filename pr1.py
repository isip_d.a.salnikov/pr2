import os
from pprint import pprint
import numpy as np

def clear():
    os.system('cls' if os.name=='nt' else 'clear')
def input_safe(type_, prompt=''):
    try:
        return type_(input(prompt))
    except ValueError:
        return input_safe(type_, prompt)

def menu(options: list, prompt=''):
    if prompt:
        print(prompt)
    for i, v in enumerate(options, 1):
        print(f'{i}: {v}')
    valid = range(1, len(options) + 1)
    index = input_safe(int, 'Select a number: ')
    if index not in valid:
        return menu(options)
    return index - 1

# 1 'калькулятор'
# необходимо реализовать все стандартные арифметические
# операции в питоне(сложение, вычитание, умножение, деление, деление на
# цело, нахождение остатка от деления, степень) , также
# добавить 4 метода из библиотеки math, расчеты проводятся на 2-х
# введенных пользователем цифрах если иное не предполагается, в случае
# неправильного ввода программа не должна критически ломаться, необходим
# вывод сообщения о неправильном вводе цифры, добавить расчет площади и
# периметра параллелепипеда используя лямбду выражения.
def calculator():
    from math import sin, cos, tan, sqrt
    print('Math functions: sin, cos, tan, sqrt')

    def perimeter(a, b, c):
        return 4 * (a + b + c)
    def area(a, b, c):
        return 2 * (a*b + b*c + c*a)
    print('Parallelepiped functions: \nperimeter(a, b, c) \narea(a, b, c)')
    expr = input('Input expression: ')
    try:
        print(f'Result: {eval(expr)}')
    except Exception:
        print('Error!\n')
    calculator()


# 2 'подсчет в строке'
# должна подсчитать кол-во символов,
# пробелов и запятых, в введённом пользователем строке.
def count_string():
    string = input("Type a string: ")
    print(f"Spaces: {string.count(' ')}")
    print(f"Commas: {string.count(',')}")


# 3 'матрица'
# необходимо написать функцию которая будет возвращать матрицу. у данной функции имеется следующие параметры:
# 1 параметр - это кол-во столбцов в матрице
# 2 параметр - это кол-во строк в матрице
# 3 параметр - это начало, т. е. с какого значения должно начинаться матрица 
# 4 параметр - это шаг, т. е. на какое число должно увеличиваться последующее значение матрицы.
def generate_matrix(n_cols, n_rows, start, step):
    flat = np.arange(start, (start + n_rows * n_cols * step), step)
    return flat.reshape((n_rows, n_cols))

def prompt_matrix():
    matrix = generate_matrix(
        input_safe(int, 'Number of cols: '),
        input_safe(int, 'Number of rows: '),
        input_safe(int, 'Start: '),
        input_safe(int, 'Step: '),
        )
    pprint(matrix)

if __name__ == "__main__":
    options = ['Calculator', 'Count spaces and commas in string', 'Generate matrix']
    funcs = [calculator, count_string, prompt_matrix]
    index = menu(options, 'Select a function')
    funcs[index]()